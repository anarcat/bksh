#! /usr/bin/perl -w

use strict;

# type => name mappings
my %names = ( 0 => "full ",
              1 => "hebdo",
              2 => "daily");
my %machines = ();

print "all sizes in MB\n";
printf "%-9s %-5s %10s %5s %10s\n", "mach/part", "type", "total", "count", "avg";

while (<>) {

  my ($size, $location) = split(/\s/, $_);

  if ($location =~ /^(.*)\/(.*)_([0-9]+)/) {
     my ($machine, $part, $type, $i) = ($1, $2, $3);
     if (!$location =~ /[^0-9][0-9]+$/) {
       $i = 0;
     } else {
       $i = $&;
     }
     if (!defined($machines{$machine})) {
       %{$machines{$machine}} = ();
     }
     if ($type > 1) {
       $type = 2; # group daily backups
     }
     if (!defined($machines{$machine}{$type})) {
       %{$machines{$machine}{$type}} = (); # size, count
     }
     if (!defined($machines{$machine}{$type}{$part})) {
       @{$machines{$machine}{$type}{$part}} = (0, 0); # size, count
     }
     my @tmp = @{$machines{$machine}{$type}{$part}};
     $tmp[0] += $size;
     $tmp[1]++;
     #print "$machine, $type, $part: (@tmp) $i\n";
     $machines{$machine}{$type}{$part} = \@tmp;
  } else {
     #print "bad du line: $_";
  }

}

while ( my ($machine, $types) = each %machines) {
  print $machine . "\n";
  my %avg;
  my $grand_total = 0;
  my %type_total;
  while ( my ($type, $parts) = each %{$types}) {
    while (my ($part, $size) = each %{$parts}) {
      my ($total, $count) = @{$size};
      $total /= 1024;
      printf "%9s %5s %10.2f %5d %10.2f\n", $part, $names{$type}, $total, $count, $total/$count;
      $grand_total += $total;
      $avg{$type} += $total/$count;
      $type_total{$type} += $total;
    }
  }
  print "totaux\n";
  while ( my ($type, $totals) = each %avg) {
    #my ($total, $count) = @{$totals};
    printf "%9s %5s %10.2f %5s %10.2f\n", "", $names{$type}, $type_total{$type},  "", $totals;
  }
  printf "    grand total: %9.2f\n\n", $grand_total;
}
