#! /bin/sh -e

# the first argument given will be appended to backup filenames
period=$1

# the backup server
server=backup.example.com
user=backup

# what to backup
# dbs to backup
db_user=test
databases="test"
# directories to backup
dirs="/tmp"

# program locations
tar=/bin/tar
tar_flags="-l"
mysqldump=/usr/bin/mysqldump
mysql_flags="--defaults-file=/etc/mysql/debian.cnf"
gzip_pipe="gzip -c -9"
gzip_ext=".gz"

. /etc/backup-tar-mysql.conf

# backup directories
$tar $tar_flags -C $HOME -f - -c $dirs | $gzip_pipe | ssh ${user}@${server} backup-$period.tar$gzip_ext

# then databases
for database in $databases
do
	# -l: read-lock tables, -q: quick (no buffering), -Q: quote table names
	$mysqldump $mysql_flags -l -q -Q -u $db_user $database | $gzip_pipe | ssh ${user}@${server} backup-${database}-${period}$gzip_ext
done
