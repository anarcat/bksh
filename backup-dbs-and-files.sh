#! /bin/sh -x

# we don't want to hear normal messages
exec > /dev/null

# the first argument given will be appended to backup filenames
period=$1

# the backup server
server=backup.example.com

# what to backup
# dbs to backup
db_user=mysql
databases="test1 test2"
# directories to backup
dirs="public_html/"

# program locations
tar=/usr/bin/tar
mysqldump=/usr/local/bin/mysqldump
gzip_pipe="gzip -c -9"
gzip_ext=".gz"

# backup directories
$tar -C $HOME -f - -c $dirs | $gzip_pipe | ssh backup@${server} backup-$period.tar$gzip_ext

# then databases
for database in $databases
do
	# -l: read-lock tables, -q: quick (no buffering), -Q: quote table names
	$mysqldump -l -q -Q -u $db_user $database | $gzip_pipe | ssh backup@${server} backup-${database}-${period}$gzip_ext
done
