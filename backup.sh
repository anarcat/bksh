#! /bin/sh

# $Id: backup.sh,v 1.14 2003/01/25 18:39:01 anarcat Exp $

BACKUP_HOST=backup
BACKUP_USER=backup

usage () {
        echo "Usage: $0 [-z | -b] [-p] level fs [fs...]"
        echo "-p: do all dumps at the same time"
        echo "-[zb]: compress on-the-fly using [gb]zip"
        echo "level: dump(8) level"
        echo "fs: filesystem to backup"
}

args=`getopt zbp $*`

if [ $? != 0 ] ; then
        usage
        exit 1
fi

set -- $args

for i
do
        case "$i"
        in
                -z)
                        compress='gzip -9 |'
                        suffix=".gz"
                        shift;;
                -b)
                        compress='bzip -9 |'
                        suffix=".bz"
                        shift;;
                -p)
                        parallel='&';
                        shift;;
                --)
                        shift; break;;
        esac
done

if [ $# -lt 2 ] ; then
       usage
fi


level=$1; shift;

if [ $level -gt 9 -o $level -lt 0 ]; then
        usage
        echo "invalid level value"
        exit 1
fi

for i
do
        fs=$1; shift
        fsname=`basename $fs`
        if [ "X${fs}" = "X/" ] ; then
                fsname=root
        fi
        dmp_cmd="dump -${level}anu -f - ${fs}"
        ssh_cmd="ssh -T ${BACKUP_USER}@${BACKUP_HOST} ${fsname}_${level}.dmp$suffix"
        eval "$dmp_cmd | $compress $ssh_cmd $parallel"
done
