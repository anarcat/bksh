.include "local.mk"

# Stolen from bsd.port.mk
# Get __FreeBSD_version
.if !defined(OSVERSION)
.if exists(/sbin/sysctl)
OSVERSION!=     /sbin/sysctl -n kern.osreldate
.else
OSVERSION!=     /usr/sbin/sysctl -n kern.osreldate
.endif
.endif

.if ${OSVERSION} < 430000
SRCS+=	basename.c dirname.c
.endif

.include <bsd.prog.mk>
