/* the directory in $HOME where backups are stored */
#define DEF_DATADIR "incoming"

/* the default backup filename when none given on cline */
#define DEF_FILENAME "backup.raw"

/* maximum number of copies of a given backup */
#define MAX_BAKS 10

/* comment do disble plain file backups */
#define FILE_BACKUP

/* choose the buffer size */
#ifndef BUFSIZ
#define BUFSIZ 8192
#else
/* you can redefine the buffer size in bytes here */
/* #define BUFSIZE 8192 */
#endif

/* maximum number of arguments in -c argument */
#define MAX_ARGS 30
