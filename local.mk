PROG=bksh

SRCS=bksh.c

BINDIR=/bin

NOMAN=yes

CFLAGS+=-Wall

PREFIX?=/usr
DESTDIR?=${PREFIX}

RELEASE=1.7
REL_TAG=BKSH_REL_${RELEASE:S/./_/g}

CLEANFILES+=	bksh-${RELEASE}/ bksh-${RELEASE}.tar.gz

all: ${PROG}

release: bksh-${RELEASE}.tar.gz


bksh-${RELEASE}.tar.gz: clean ${PROG}-${RELEASE}
	@echo "Building release tarball"
	@tar -v -c -z --exclude CVS -f $@ ${PROG}-${RELEASE}
	@md5 $@

${PROG}-${RELEASE}: ChangeLog
	@echo "Checking out temp copy of release ${REL_TAG}"
	@echo "If you forgot to tag this release, use the tag target"
	@cvs co -d $@ -r${REL_TAG} ${PROG} > /dev/null
	@cp ChangeLog $@

tag:
	@echo "Tagging ${REL_TAG}"
	@cvs tag ${REL_TAG} || true

ChangeLog::
	@cvs2cl -r -b -t --no-wrap -S 2> /dev/null
